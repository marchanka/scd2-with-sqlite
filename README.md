# SCD2 with sqlite

Slowly Changing Dimension Type 2.

DIM_REQUEST_HIS - table with historical data. Every change of the natural key is a new line with reference to the previous historical status.

DIM_REQUEST_V_ACT - view shows the last status for every natural key. The view is needed for ETL.

DIM_REQUEST_V_SCD - the SCD2 view with attributes like active_from, active_to and delete_flag. The unique combination is NK + active to.

STG_REQUEST_META - sample of master data we want to historize

STG_ACCESS_LOG - sample of source, which contain also the natural keys we want to historize

Extract transform and load procedures:

ETL_DIM_REQUEST_INSERT

ETL_DIM_REQUEST_UPDATE

ETL_DIM_REQUEST_DELETE

ETL_DIM_REQUEST_PROPAGATE_NK
