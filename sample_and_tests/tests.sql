
/* initial data */
/* master data */
INSERT INTO STG_REQUEST_META (TITLE,NAME,DESCRIPTION,KEYWORDS) VALUES
('Home Page','home_page','',''),
('Contact Form','contact','Feedback form','contact, feedback, imprint');
/* access log */
INSERT INTO STG_ACCESS_LOG VALUES
('1','141.8.188.1','[31/Mar/2020:01:51:48 +0200]','/robots.txt','HTTP/1.1','200','886'),
('2','141.8.188.34','[31/Mar/2020:02:51:52 +0200]','/content/image.png','HTTP/1.1','200','5239'),
('7','66.249.64.2','[31/Mar/2020:03:20:28 +0200]','/files/site_default.css','HTTP/1.1','404','1258'),
('9','66.249.64.1','[31/Mar/2020:03:20:29 +0200]','/files/site_print.css','HTTP/1.1','404','1257'),
('10','188.165.200.5','[31/Mar/2020:03:45:06 +0200]','/contact','HTTP/1.1','200','11924'),
('11','142.93.191.2','[31/Mar/2020:03:55:17 +0200]','/','HTTP/1.1','200','3998'),
('12','142.93.191.2','[31/Mar/2020:05:55:17 +0200]','/libs/lytebox/lytebox.js','HTTP/1.1','200','14791'),
('13','141.8.188.34','[31/Mar/2020:06:58:22 +0200]','/home_page','HTTP/1.1','200','3998'),
('14','108.62.3.4','[31/Mar/2020:09:59:42 +0200]','/contact','HTTP/1.0','200','11924');

/* run ETL */
ETL_DIM_REQUEST_INSERT.sql
ETL_DIM_REQUEST_UPDATE.sql
ETL_DIM_REQUEST_DELETE.sql
ETL_DIM_REQUEST_PROPAGATE_NK.sql

/* update master data */
INSERT INTO STG_REQUEST_META (TITLE,NAME,DESCRIPTION,KEYWORDS) VALUES
('robots.txt','robots.txt','robots','robots'),
('New Page','new_one','','new');
UPDATE STG_REQUEST_META SET DESCRIPTION = 'Page Description', KEYWORDS = 'home, page, description' WHERE NAME='home_page';

/* run ETL */
ETL_DIM_REQUEST_INSERT.sql
ETL_DIM_REQUEST_UPDATE.sql
ETL_DIM_REQUEST_DELETE.sql
ETL_DIM_REQUEST_PROPAGATE_NK.sql

/* delete from master data */
DELETE FROM STG_REQUEST_META WHERE NAME='robots.txt';

/* run ETL */
ETL_DIM_REQUEST_INSERT.sql
ETL_DIM_REQUEST_UPDATE.sql
ETL_DIM_REQUEST_DELETE.sql
ETL_DIM_REQUEST_PROPAGATE_NK.sql

/* delete from master data */
DELETE FROM STG_REQUEST_META WHERE NAME='new_one';

/* run ETL */
ETL_DIM_REQUEST_INSERT.sql
ETL_DIM_REQUEST_UPDATE.sql
ETL_DIM_REQUEST_DELETE.sql
ETL_DIM_REQUEST_PROPAGATE_NK.sql

